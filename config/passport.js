const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET
}, function(access_Token, refreshToken, profile, done){
    try{
        Usuario.findOneOrCreateByFacebook(profile, function(err, user){
            if(err){ console.log('err '+err);
            return done(err, user);
        }
        });
    }catch(err2){
        console.log(err2);
        return done(err2, null);
    }
}))

passport.use(new LocalStrategy(
    function(email, password, done) { 
        Usuario.findOne({ email: email }, function (err, usuario) {
            if (err) { return done(err); }
            if (!usuario) {
                return done(null, false, { message: 'Usuario incorrecto.' }); 
            }
            if (!usuario.validPassword(password)) {
                return done(null, false, { message: 'Password incorrecto.' });
            }
            return done(null, usuario);
        });
    }
));
//se configura la estrategia de google
passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST+"/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
      console.log(profile);
    Usuario.findOneOrCreateByGoogle({ googleId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  })
);
/**
 * asi se explica en la documentacion
 * passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "http://www.example.com/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
));
 */
passport.serializeUser(function(user, cb){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});


module.exports = passport;