var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', () => {

    describe('GET BICICLETAS /', () => {
        it('Status 200',() => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.60,-58.30]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200',(done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url:     'http://localhost:3000/api/bicicletas/create',
                body:    aBici
              }, function(error, response, body) {
                  expect(response.statusCode).toBe(200);
                  expect(Bicicleta.findById(10).color).toBe("rojo");
                  done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('STATUS 201',(done) => {
            var a = new Bicicleta(11, 'rojo', 'urbana', [-34.60,-58.30]);
            Bicicleta.add(a);
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":11, "color": "verde", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url:     'http://localhost:3000/api/bicicletas/update',
                body:    aBici
              }, function(error, response, body) {
                  expect(response.statusCode).toBe(201);
                  expect(Bicicleta.findById(11).color).toBe("verde");
                  done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('STATUS 204',(done) => {
            var a = new Bicicleta(12, 'rojo', 'urbana', [-34.60,-58.30]);
            Bicicleta.add(a);
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":12}';
            request.delete({
                headers: headers,
                url:     'http://localhost:3000/api/bicicletas/delete',
                body:    aBici
              }, function(error, response, body) {
                  expect(response.statusCode).toBe(204);
                  done();
            });
        });
    });
});